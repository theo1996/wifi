package com.example.eleve.androidco;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiManager;
import android.net.wifi.ScanResult;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.util.List;

import static android.net.wifi.WifiManager.SCAN_RESULTS_AVAILABLE_ACTION;


public class WIFIActivity extends AppCompatActivity {

    private WifiManager wifiManager;
    private BroadcastReceiver wifiReceiver;
    private ArrayAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wifi);

        ListView listView =(ListView) findViewById(R.id.listView);

        adapter = new ArrayAdapter<>
                (this, android.R.layout.simple_list_item_1);

        listView.setAdapter(adapter);
        wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        wifiReceiver = new WiFiScanReceiver();
    }

    public void onToggleClicked(View view) {
        adapter.clear();
        ToggleButton toggleButton = (ToggleButton) view;
        if (wifiManager == null) {
            Toast.makeText(getApplicationContext(), "le wifi n'est pas supporté", Toast.LENGTH_SHORT).show();
            toggleButton.setChecked(false);

        } else {
            if (toggleButton.isChecked()) {
                if (!wifiManager.isWifiEnabled()) {
                    Toast.makeText(getApplicationContext(), "le wifi est  allumé" + " \n" + "Scanner en cours...", Toast.LENGTH_SHORT).show();
                    wifiManager.setWifiEnabled(true);

                } else {
                    Toast.makeText(getApplicationContext(), "le wifi est déja allumé en on" + "\n" + "scanner en cours...", Toast.LENGTH_SHORT).show();

                }
                Log.d("testcoucou",String.valueOf(wifiManager.startScan()));
                wifiManager.startScan();
                Toast.makeText(getApplicationContext(), "scan fini", Toast.LENGTH_SHORT).show();
onResume();


            } else {
                Toast.makeText(getApplicationContext(), "le wifi est off", Toast.LENGTH_SHORT).show();
                wifiManager.setWifiEnabled(false);
            }
        }
    }

    class WiFiScanReceiver extends BroadcastReceiver {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (SCAN_RESULTS_AVAILABLE_ACTION.equals(action)) {
                Log.d("testcoucou",String.valueOf(wifiManager.getScanResults()));
                List<ScanResult> wifiScanResultList = wifiManager.getScanResults();

                Log.d("testcoucou",String.valueOf(wifiScanResultList.size()));
                for (int i = 0; i < wifiScanResultList.size(); i++) {
                    ScanResult accessPoint = wifiScanResultList.get(i);
                    String listItem = accessPoint.SSID +"\n" + accessPoint.BSSID;
                    adapter.add(listItem);
                    Log.d("test",listItem);

                }
            }else{
                Log.d("testcoucou","coucou2");
            }
            onPause();
        }
    }
    protected void onPause() {
        super.onPause();
        unregisterReceiver(wifiReceiver);
    }
    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter(SCAN_RESULTS_AVAILABLE_ACTION);
        registerReceiver(wifiReceiver, filter);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.wi_fi, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {

            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}